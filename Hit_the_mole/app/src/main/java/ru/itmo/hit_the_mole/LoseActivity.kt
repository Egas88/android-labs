package ru.itmo.hit_the_mole

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class LoseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lose)
        val againButton = findViewById<Button>(R.id.AgainButton)
        againButton.setOnClickListener {
            val intent = Intent(this@LoseActivity, MainActivity::class.java)
            startActivity(intent)
        }
    }
}